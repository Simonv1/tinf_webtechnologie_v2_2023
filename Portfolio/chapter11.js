function berechneBMI() {
    var groesseEingabe = document.getElementById('groesse');
    var gewichtEingabe = document.getElementById('gewicht');
    var ergebnisDiv = document.getElementById('ergebnis');

    var groesse = parseFloat(groesseEingabe.value);
    var gewicht = parseFloat(gewichtEingabe.value);

    if (isNaN(groesse) || isNaN(gewicht)) {
        ergebnisDiv.textContent = 'Bitte geben Sie eine gültige Körpergröße und Gewicht ein.';
        return;
    }

    var bmi = gewicht / ((groesse / 100) ** 2);
    bmi = bmi.toFixed(2);

    var category = '';
    if (bmi < 18.5) {
        category = 'Untergewicht';
    } else if (bmi < 24.9) {
        category = 'Normalgewicht';
    } else if (bmi < 29.9) {
        category = 'Übergewicht';
    } else {
        category = 'Adipositas';
    }

    ergebnisDiv.textContent = 'Ihr BMI ist ' + bmi + ' (' + category + ').';
}